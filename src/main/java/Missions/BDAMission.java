package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.BDACapability;
import AerialVehicles.Capabilities.BDAModule;
import Entities.Coordinates;

public class BDAMission extends Mission{
    private final String objective;
    private final BDAModule bdaModule;

    public BDAMission(
        Coordinates destination,
        String pilotName,
        AerialVehicle aircraft,
        String objective)
        throws AerialVehicleNotCompatibleException
    {
        super(destination, pilotName, aircraft);
        if(!(aircraft instanceof BDACapability)){
            throw new AerialVehicleNotCompatibleException("Not suitable for bda mission");
        }
        this.objective = objective;
        this.bdaModule = ((BDACapability) aircraft).getBDAModule();
    }

    @Override
    public String executeMission() {
        return
            pilotName
            + ": "
            + aircraft.getAircraftName()
            + " taking pictures of suspect "
            + objective
            + " with: "
            + bdaModule.getCameraKind()
            + " camera";
    }
}
