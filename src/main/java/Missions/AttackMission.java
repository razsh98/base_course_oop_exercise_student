package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.CombatCapability;
import AerialVehicles.Capabilities.CombatModule;
import Entities.Coordinates;

public class AttackMission extends Mission{
    private final String target;
    private final CombatModule combatModule;

    public AttackMission(
        Coordinates destination,
        String pilotName,
        AerialVehicle aircraft,
        String target)
        throws AerialVehicleNotCompatibleException
    {
        super(destination, pilotName, aircraft);
        if(!(aircraft instanceof CombatCapability)){
            throw new AerialVehicleNotCompatibleException("Not suitable for attack mission");
        }
        this.target = target;
        this.combatModule = ((CombatCapability) aircraft).getCombatModule();
    }

    @Override
    public String executeMission() {
        return
            pilotName
            + ": "
            + aircraft.getAircraftName()
            + " Attacking suspect "
            + target
            + " with: "
            + combatModule.getArmamentKind()
            + "X"
            + combatModule.getNumOfArmaments();
    }
}
