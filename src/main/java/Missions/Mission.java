package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission{
    protected Coordinates destination;
    protected String pilotName;
    protected AerialVehicle aircraft;

    public Mission(Coordinates destination, String pilotName, AerialVehicle aircraft) {
        this.destination = destination;
        this.pilotName = pilotName;
        this.aircraft = aircraft;
    }

    public void begin(){
        System.out.println("Beginning Mission!");
        aircraft.flyTo(destination);
    }

    public void cancel(){
        System.out.println("Abort Mission!");
        aircraft.land(aircraft.getHomeBase());
    }

    public void finish(){
        System.out.println(executeMission());
        aircraft.land(aircraft.getHomeBase());
        System.out.println("Finish Mission!");
    }

    public abstract String executeMission();
}
