package Missions;

import AerialVehicles.AerialVehicle;
import AerialVehicles.Capabilities.IntelligenceCapability;
import AerialVehicles.Capabilities.IntelligenceModule;
import Entities.Coordinates;

public class IntelligenceMission extends Mission{
    private final String region;
    private final IntelligenceModule intelligenceModule;

    public IntelligenceMission(
        Coordinates destination,
        String pilotName,
        AerialVehicle aircraft,
        String region)
        throws AerialVehicleNotCompatibleException
    {
        super(destination, pilotName, aircraft);
        if(!(aircraft instanceof IntelligenceCapability)){
            throw new AerialVehicleNotCompatibleException("Not suitable for intelligence mission");
        }
        this.region = region;
        this.intelligenceModule = ((IntelligenceCapability) aircraft).getIntelligenceModule();
    }

    @Override
    public String executeMission() {
        return
            pilotName
            + ": "
            + aircraft.getAircraftName()
            + " Collecting Data in "
            + region
            + " with: sensor type: "
            + intelligenceModule.getSensorKind();
    }
}
