import AerialVehicles.Capabilities.BDAModule;
import AerialVehicles.Capabilities.CombatModule;
import AerialVehicles.Capabilities.IntelligenceModule;
import AerialVehicles.Enums.*;
import AerialVehicles.F15;
import AerialVehicles.F16;
import AerialVehicles.Zik;
import Entities.Coordinates;
import Missions.*;

public class Main {
    public static void main(String[] args){
        try {
            //<editor-fold desc="Aircraft Definitions">
            Zik zik = new Zik(
                0,
                FlightStatus.READY,
                new Coordinates(30.823521,35.242314),
                new BDAModule(CameraKind.REGULAR),
                new IntelligenceModule(SensorKind.ELINT));

            F15 abireiHazanavHakaful = new F15(
                0,
                FlightStatus.READY,
                new Coordinates(31.127424,35.026472),
                new CombatModule(3, ArmamentKind.AMRAM),
                new IntelligenceModule(SensorKind.INFRARED)
            );

            F16 abireiHazanavHakatom = new F16(
                300,
                FlightStatus.READY,
                new Coordinates(30.462745,35.172947),
                new CombatModule(2, ArmamentKind.SPICE250),
                new BDAModule(CameraKind.NIGHT_VISION)
            );
            //</editor-fold>

            //<editor-fold desc="Mission Definitions">
            AttackMission pewpewpew = new AttackMission(
                new Coordinates(20.728498,37.345325),
                "Ilan Ramon",
                abireiHazanavHakatom,
                "Furrytown"
            );

            BDAMission pshpshpsh = new BDAMission(
                new Coordinates(25.187459,46.289479),
                "Ezio Auditore Da Firenze",
                zik,
                "Pizza Pasta Linguini"
            );

            IntelligenceMission woooooosh = new IntelligenceMission(
                new Coordinates(27.836758,84.372375),
                "Naoshi Kanno",
                abireiHazanavHakaful,
                "Iwo Jima"
            );
            //</editor-fold>

            pewpewpew.begin();
            pewpewpew.finish();

            pshpshpsh.begin();
            pshpshpsh.finish();

            woooooosh.begin();
            woooooosh.cancel();

        } catch (AerialVehicleNotCompatibleException e) {
            e.printStackTrace();
        }

    }
}
