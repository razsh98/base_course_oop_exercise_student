package AerialVehicles;


import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public abstract class AerialVehicle {
    protected int timeSinceLastMaintenance;
    protected FlightStatus flightStatus;
    protected Coordinates homeBase;

    public AerialVehicle(int timeSinceLastMaintenance, FlightStatus flightStatus, Coordinates homeBase) {
        this.timeSinceLastMaintenance = timeSinceLastMaintenance;
        this.flightStatus = flightStatus;
        this.homeBase = homeBase;
    }

    public Coordinates getHomeBase() {
        return homeBase;
    }

    private boolean isReady(){
        return flightStatus == FlightStatus.READY;
    }

    public void setFlightStatus(FlightStatus flightStatus) {
        this.flightStatus = flightStatus;
    }

    private void resetTimeSinceLastMaintenance() {
        this.timeSinceLastMaintenance = 0;
    }

    public void flyTo(Coordinates destination){
        if(isReady()){
            System.out.println("Flying to: " + destination.toString());
            setFlightStatus(FlightStatus.MID_FLIGHT);
        }
        else{
            System.out.println("Aerial Vehicle isn't ready to fly");
        }
    }

    public void land(Coordinates destination){
        System.out.println("Landing on: " + destination.toString());
        check();
    }

    private void check(){
        if(timeSinceLastMaintenance >= getMaxTimeUntilMaintenance()){
            setFlightStatus(FlightStatus.NOT_READY);
            repair();
        }
        else{
            setFlightStatus(FlightStatus.READY);
        }
    }

    private void repair(){
        resetTimeSinceLastMaintenance();
        setFlightStatus(FlightStatus.READY);
    }

    public abstract int getMaxTimeUntilMaintenance();

    public abstract String getAircraftName();
}
