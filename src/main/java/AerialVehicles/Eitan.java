package AerialVehicles;


import AerialVehicles.Capabilities.CombatCapability;
import AerialVehicles.Capabilities.CombatModule;
import AerialVehicles.Capabilities.IntelligenceCapability;
import AerialVehicles.Capabilities.IntelligenceModule;
import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public class Eitan extends Drone implements CombatCapability, IntelligenceCapability {
    private final CombatModule combatModule;
    private final IntelligenceModule intelligenceModule;

    public Eitan(
        int timeSinceLastMaintenance,
        FlightStatus flightStatus,
        Coordinates homeBase,
        CombatModule combatModule,
        IntelligenceModule intelligenceModule)
    {
        super(timeSinceLastMaintenance, flightStatus, homeBase);
        this.combatModule = combatModule;
        this.intelligenceModule = intelligenceModule;
    }

    @Override
    public int getMaxTimeUntilMaintenance() {
        return Config.HARON_MAX_TIME_UNTIL_MAINTENANCE;
    }

    @Override
    public String getAircraftName() { return "Eitan"; }

    @Override
    public CombatModule getCombatModule() { return combatModule; }

    @Override
    public IntelligenceModule getIntelligenceModule() { return intelligenceModule; }
}
