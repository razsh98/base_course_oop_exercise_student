package AerialVehicles.Capabilities;

import AerialVehicles.Enums.ArmamentKind;

public class CombatModule {
    private final int numOfArmaments;
    private final ArmamentKind armamentKind;

    public CombatModule(int numOfArmaments, ArmamentKind armamentKind) {
        this.numOfArmaments = numOfArmaments;
        this.armamentKind = armamentKind;
    }

    public int getNumOfArmaments() {
        return numOfArmaments;
    }

    public ArmamentKind getArmamentKind() {
        return armamentKind;
    }
}
