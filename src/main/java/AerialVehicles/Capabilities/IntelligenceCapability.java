package AerialVehicles.Capabilities;

public interface IntelligenceCapability {
    IntelligenceModule getIntelligenceModule();
}
