package AerialVehicles.Capabilities;

import AerialVehicles.Enums.SensorKind;

public class IntelligenceModule {
    private final SensorKind sensorKind;

    public IntelligenceModule(SensorKind sensorKind) {
        this.sensorKind = sensorKind;
    }

    public SensorKind getSensorKind() {
        return sensorKind;
    }
}
