package AerialVehicles.Capabilities;

public interface BDACapability {
    BDAModule getBDAModule();
}
