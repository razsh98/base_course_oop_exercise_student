package AerialVehicles.Capabilities;

import AerialVehicles.Enums.CameraKind;

public class BDAModule {
    private final CameraKind cameraKind;

    public BDAModule(CameraKind cameraKind) {
        this.cameraKind = cameraKind;
    }

    public CameraKind getCameraKind() {
        return cameraKind;
    }
}
