package AerialVehicles.Capabilities;

public interface CombatCapability {
    CombatModule getCombatModule();
}
