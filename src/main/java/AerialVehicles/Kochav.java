package AerialVehicles;


import AerialVehicles.Capabilities.*;
import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public class Kochav extends Drone implements CombatCapability, BDACapability, IntelligenceCapability {
    private final CombatModule combatModule;
    private final BDAModule bdaModule;
    private final IntelligenceModule intelligenceModule;

    public Kochav(
        int timeSinceLastMaintenance,
        FlightStatus flightStatus,
        Coordinates homeBase,
        CombatModule combatModule,
        BDAModule bdaModule,
        IntelligenceModule intelligenceModule)
    {
        super(timeSinceLastMaintenance, flightStatus, homeBase);
        this.combatModule = combatModule;
        this.bdaModule = bdaModule;
        this.intelligenceModule = intelligenceModule;
    }

    @Override
    public int getMaxTimeUntilMaintenance() {
        return Config.HERMES_MAX_TIME_UNTIL_MAINTENANCE;
    }

    @Override
    public String getAircraftName() { return "Kochav"; }

    @Override
    public CombatModule getCombatModule() { return combatModule; }

    @Override
    public BDAModule getBDAModule() { return bdaModule; }

    @Override
    public IntelligenceModule getIntelligenceModule() { return intelligenceModule; }
}
