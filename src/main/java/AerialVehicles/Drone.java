package AerialVehicles;

import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public abstract class Drone extends AerialVehicle{
    public Drone(int timeSinceLastMaintenance, FlightStatus flightStatus, Coordinates homeBase) {
        super(timeSinceLastMaintenance, flightStatus, homeBase);
    }

    public String hoverOverLocation(Coordinates destination){
        setFlightStatus(FlightStatus.MID_FLIGHT);
        String res = destination.toString();
        System.out.println(res);
        return res;
    }
}
