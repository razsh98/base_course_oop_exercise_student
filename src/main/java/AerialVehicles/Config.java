package AerialVehicles;

public class Config {
    public static int PLANES_MAX_TIME_UNTIL_MAINTENANCE = 250;
    public static int HARON_MAX_TIME_UNTIL_MAINTENANCE = 150;
    public static int HERMES_MAX_TIME_UNTIL_MAINTENANCE = 100;
}
