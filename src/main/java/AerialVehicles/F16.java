package AerialVehicles;


import AerialVehicles.Capabilities.BDACapability;
import AerialVehicles.Capabilities.BDAModule;
import AerialVehicles.Capabilities.CombatCapability;
import AerialVehicles.Capabilities.CombatModule;
import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public class F16 extends AerialVehicle implements CombatCapability, BDACapability {
    private final CombatModule combatModule;
    private final BDAModule bdaModule;

    public F16(
        int timeSinceLastMaintenance,
        FlightStatus flightStatus,
        Coordinates homeBase,
        CombatModule combatModule,
        BDAModule bdaModule)
    {
        super(timeSinceLastMaintenance, flightStatus, homeBase);
        this.combatModule = combatModule;
        this.bdaModule = bdaModule;
    }

    @Override
    public int getMaxTimeUntilMaintenance() {
        return Config.PLANES_MAX_TIME_UNTIL_MAINTENANCE;
    }

    @Override
    public String getAircraftName() { return "F16"; }

    @Override
    public CombatModule getCombatModule() {
        return combatModule;
    }

    @Override
    public BDAModule getBDAModule() {
        return bdaModule;
    }

}
