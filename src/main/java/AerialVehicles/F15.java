package AerialVehicles;


import AerialVehicles.Capabilities.CombatCapability;
import AerialVehicles.Capabilities.CombatModule;
import AerialVehicles.Capabilities.IntelligenceCapability;
import AerialVehicles.Capabilities.IntelligenceModule;
import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public class F15 extends AerialVehicle implements CombatCapability, IntelligenceCapability {
    private final CombatModule combatModule;
    private final IntelligenceModule intelligenceModule;

    public F15(
        int timeSinceLastMaintenance,
        FlightStatus flightStatus,
        Coordinates homeBase,
        CombatModule combatModule,
        IntelligenceModule intelligenceModule)
    {
        super(timeSinceLastMaintenance, flightStatus, homeBase);
        this.combatModule = combatModule;
        this.intelligenceModule = intelligenceModule;
    }

    @Override
    public int getMaxTimeUntilMaintenance() {
        return Config.PLANES_MAX_TIME_UNTIL_MAINTENANCE;
    }

    @Override
    public String getAircraftName() { return "F15"; }

    @Override
    public CombatModule getCombatModule() { return combatModule; }

    @Override
    public IntelligenceModule getIntelligenceModule() { return intelligenceModule; }
}
