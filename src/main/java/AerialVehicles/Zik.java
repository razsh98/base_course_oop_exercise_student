package AerialVehicles;


import AerialVehicles.Capabilities.BDACapability;
import AerialVehicles.Capabilities.BDAModule;
import AerialVehicles.Capabilities.IntelligenceCapability;
import AerialVehicles.Capabilities.IntelligenceModule;
import AerialVehicles.Enums.FlightStatus;
import Entities.Coordinates;

public class Zik extends Drone implements BDACapability, IntelligenceCapability {
    private final BDAModule bdaModule;
    private final IntelligenceModule intelligenceModule;

    public Zik(
        int timeSinceLastMaintenance,
        FlightStatus flightStatus,
        Coordinates homeBase,
        BDAModule bdaModule,
        IntelligenceModule intelligenceModule)
    {
        super(timeSinceLastMaintenance, flightStatus, homeBase);
        this.bdaModule = bdaModule;
        this.intelligenceModule = intelligenceModule;
    }

    @Override
    public int getMaxTimeUntilMaintenance() {
        return Config.HERMES_MAX_TIME_UNTIL_MAINTENANCE;
    }

    @Override
    public String getAircraftName() { return "Zik"; }

    @Override
    public BDAModule getBDAModule() { return bdaModule; }

    @Override
    public IntelligenceModule getIntelligenceModule() { return intelligenceModule; }
}