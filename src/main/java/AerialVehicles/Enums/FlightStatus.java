package AerialVehicles.Enums;

public enum FlightStatus {
    READY,
    NOT_READY,
    MID_FLIGHT
}
