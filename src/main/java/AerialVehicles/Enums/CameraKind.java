package AerialVehicles.Enums;

public enum CameraKind {
    REGULAR,
    THERMAL,
    NIGHT_VISION
}
